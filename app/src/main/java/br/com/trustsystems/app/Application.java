package br.com.trustsystems.app;

import br.com.trustsystems.api.Add;
import br.com.trustsystems.api.AddImpl;

public class Application {
    public static void main(String[] args) {
        Add add = new AddImpl();
        final int result = add.add(1, 2);

        System.out.println("The result is "+result);
    }
}
